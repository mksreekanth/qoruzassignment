<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Task;
use App\SubTask;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTaskControllerTest extends TestCase
{
    public function testRequiredFieldsForCreate()
    {
        $this->post('api/tasks', [])
            ->assertStatus(422)
            ->assertJson([
                "success" => "false",
                "data" => [
                    "message" => "validation failed",
                    "errors" => []
                ]
            ]);
    }

    public function testCanCreateNewTask(){
        $this->post('api/tasks', $this->getTaskCreateData())
            ->assertStatus(201)
            ->assertJson([
                "success" => "true",
                "data" => [
                    "message" => "Task created"
                ],
            ]);
    }

    public function testCanUpdateTask(){
        $task = factory(Task::class)->create($this->getTaskCreateData());
        $updateData = [
            'title' => $this->faker->sentence,
            'due_date' => $this->faker->dateTimeBetween('+0 days', '+5 days')->format('Y-m-d'),
            'status' => $this->faker->randomElement(['pending' ,'completed']),
        ];
        $this->put(route('api.tasks.update', $task->id), $updateData)
            ->assertStatus(200)
            ->assertJson([
                "success" => 'true',
                "data" => [
                    "message" => "Task updated successfully"
                ]
            ]);
    }

    public function testCanShowTask() {
        $task = factory(Task::class)->create($this->getTaskCreateData());

        $this->get(route('api.tasks.show', $task->id))
            ->assertStatus(200);
    }

    public function testCanDeleteTask() {

        $task = factory(Task::class)->create($this->getTaskCreateData());

        $this->delete(route('api.tasks.delete', $task->id))
            ->assertStatus(204);
    }

    public function testCanListTasks() {
        $selectFields = [
            Task::FIELD_NAME_TITLE,
            Task::FIELD_NAME_DUE_DATE,
            Task::FIELD_NAME_STATUS,
            Task::FIELD_NAME_CREATED_AT,
            Task::FIELD_NAME_UPDATED_AT,
        ];
        $tasks = Task::select($selectFields)->get();
        $this->get(route('api.tasks'))
            ->assertStatus(200)
            ->assertJson($tasks->toArray())
            ->assertJsonStructure([
                '*' => $selectFields,
            ]);
    }

    public function testCanListSubTasks(){
        $taskIDs = Task::inRandomOrder()
            ->limit(1)
            ->pluck('id');
        $subTaskSelectFields = [
            SubTask::FIELD_NAME_TITLE,
            SubTask::FIELD_NAME_DUE_DATE,
            SubTask::FIELD_NAME_STATUS,
        ];
        $subTasks = SubTask::where('task_id', '=', $taskIDs[0])->select($subTaskSelectFields)->get();

        $this->get(route('api.tasks.show', ($taskIDs[0])))
            ->assertStatus(200)
            ->assertJson($subTasks->toArray())
            ->assertJsonStructure([
                '*' => $subTaskSelectFields,
            ]);
    }


    private function getTaskCreateData(){
         return [
            'title' => $this->faker->sentence,
            'due_date' => $this->faker->dateTimeBetween('+0 days', '+3 days')->format('Y-m-d'),
            'status' => $this->faker->randomElement(['pending' ,'completed']),
        ];
    }
}
