<?php

use Illuminate\Database\Seeder;
use App\Task;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Task::create([
                'title' => $faker->sentence,
                'due_date' => $faker->dateTimeBetween('-15 days', '+15 days'),
                'status' => $faker->randomElement(['pending' ,'completed']),
            ]);
        }
    }
}
