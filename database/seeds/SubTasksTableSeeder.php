<?php

use Illuminate\Database\Seeder;
use App\SubTask;
use App\Task;

class SubTasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $taskIds = Task::all()->pluck("id");

        for ($i = 0; $i < 150; $i++) {
            SubTask::create([
                'title' => $faker->sentence,
                'due_date' => $faker->dateTimeBetween('+15 days', '+15 days'),
                'status' => $faker->randomElement(['pending' ,'completed']),
                'task_id' => $faker->randomElement($taskIds),
            ]);
        }
    }
}
