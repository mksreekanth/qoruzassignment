

## Instructions


- [Clone the repository).
- [Run migrations using command "php artisan migrate").
- [Seed the DB using command "php artisan db:seed").
- [Can be tested using "composer test").

