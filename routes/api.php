<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('tasks',  ['as' => 'api.tasks', 'uses' => 'Api\ApiTaskController@getTasksAll']);
Route::get('tasks/{id}', ['as' => 'api.tasks.show', 'uses' => 'Api\ApiTaskController@getTask']);
Route::post('tasks', ['as' => 'api.tasks.create', 'uses' => 'Api\ApiTaskController@createTask']);
Route::put('tasks/{id}', ['as' => 'api.tasks.update', 'uses' => 'Api\ApiTaskController@updateTask']);
Route::delete('tasks/{id}',['as' => 'api.tasks.delete', 'uses' => 'Api\ApiTaskController@softDeleteTask']);
Route::get('tasks/{id}/subtasks',['as' => 'api.tasks.subtasks.list', 'uses' => 'Api\ApiTaskController@getSubTasks']);
