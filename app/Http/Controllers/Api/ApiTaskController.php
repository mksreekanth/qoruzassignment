<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
final class ApiTaskController extends Controller
{

    /**
     * @return JsonResponse
     */
    final public function getTasksAll(): JsonResponse {
        $selectFields = $this->getTaskFieldsToSelect();
        $tasks = Task::select($selectFields)->get();
        return response()->json($tasks, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    final public function createTask(Request $request): JsonResponse {
        $validator = $this->getValidatorForCreate($request);
        if($validator->fails()) {
            return response()->json([
                "success" => 'false',
                "data" => [
                    "message" => "validation failed",
                    "errors" => $validator->errors(),
                    ],
            ], 422);
        }

        try{
            $task = Task::create($request->all());
            return response()->json([
                "success" => 'true',
                "data" => [
                    "message" => "Task created"
                ],
            ],201);
        }
        catch(Exception $e){
            return response()->json([
                "success" => 'false',
                "data" => [
                    "message" => "Unable to create task"
                ],
            ], 400);
        }
    }

    /**
     * @param int $taskId
     * @return JsonResponse
     */
    final public function getTask(int $taskId): JsonResponse {
        if (!$this->taskExistsWithId($taskId)) {
            return $this->getResponseNotFound();
        }
        $selectFields = $this->getTaskFieldsToSelect();
        $task = Task::where('id', $taskId)->select($selectFields)->get();
        return response()->json($task, 200);
    }


    /**
     * @param Request $request
     * @param int $taskId
     * @return JsonResponse
     */
    final public function updateTask(Request $request, int $taskId): JsonResponse {
        if (!$this->taskExistsWithId($taskId)) {
            return $this->getResponseNotFound();
        }
        $task = Task::find($taskId);
        $this->updateTaskFields($task, $request);
        $task->save();
        return response()->json([
                "success" => 'true',
                "data" => [
                "message" => "Task updated successfully"
            ],
        ], 200);
    }

    /**
     * @param int $taskId
     * @return Response
     */
    final public function softDeleteTask (int $taskId): Response {
        Task::destroy($taskId);
        return response("", 204);
    }

    /**
     * @param int $taskId
     * @return JsonResponse
     */
    final public function getSubTasks(int $taskId): JsonResponse{
        if (!$this->taskExistsWithId($taskId)) {
            return $this->getResponseNotFound();
        }
        $task = Task::find($taskId);
        $subTasks = $task->getSubTasksBasicInfo()->get();
        return response()->json($subTasks, 200);
    }

    private function updateTaskFields(Task $task, Request $request): void {
        foreach ($this->getTaskFieldsToUpdate() as $id => $field){
            if (is_null($request->$field) ){
                continue;
            }
            $task->$field = $request->$field;
        }

    }

    private function taskExistsWithId(int $taskId): bool{
        return Task::where('id', $taskId)->exists();
    }

    private function getValidatorForCreate(Request $request): \Illuminate\Contracts\Validation\Validator{
        return  Validator::make($request->all(),$this->getTaskValidationRules());
    }

    private function getTaskValidationRules(): array {
        return [
            Task::FIELD_NAME_TITLE => 'required|min:6',
            Task::FIELD_NAME_DUE_DATE => 'required|date_format:Y-m-d|after:today',
            Task::FIELD_NAME_STATUS => 'required|in:pending,completed',
        ];
    }

    private function getTaskFieldsToSelect(): array {
        return [
            Task::FIELD_NAME_TITLE,
            Task::FIELD_NAME_DUE_DATE,
            Task::FIELD_NAME_STATUS,
            Task::FIELD_NAME_CREATED_AT,
            Task::FIELD_NAME_UPDATED_AT,
        ];
    }

    private function getTaskFieldsToUpdate(): array {
        return [
            Task::FIELD_NAME_TITLE,
            Task::FIELD_NAME_DUE_DATE,
            Task::FIELD_NAME_STATUS,
        ];
    }

    private function getResponseNotFound(): JsonResponse {
        $data = [
            'success' => false,
            "data" => [
                "message" => "Task not found"
            ],
        ];
        return response()->json($data, 404);
    }
}
