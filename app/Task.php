<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;


class Task extends Model
{
    use SoftDeletes;

    const FIELD_NAME_TITLE = "title";
    const FIELD_NAME_DUE_DATE = "due_date";
    const FIELD_NAME_STATUS = "status";
    const FIELD_NAME_CREATED_AT = "created_at";
    const FIELD_NAME_UPDATED_AT = "updated_at";

    const ORDER_BY_FIELD_DEFAULT = "due_date";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'due_date'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy(static::ORDER_BY_FIELD_DEFAULT, 'asc');
        });
    }

    public function subTasks()
    {
        return $this->hasMany('App\SubTask');
    }

    public function getSubTasksBasicInfo()
    {
        $selectFields = [
            SubTask::FIELD_NAME_TITLE,
            SubTask::FIELD_NAME_DUE_DATE,
            SubTask::FIELD_NAME_STATUS,
        ];
        return $this->subTasks()->select($selectFields);
    }

}
