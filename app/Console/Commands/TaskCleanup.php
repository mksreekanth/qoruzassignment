<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;
use Carbon\Carbon;

class TaskCleanup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup old tasks which are soft deleted 30 days ago';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        do {
            $deleted = Task::where( 'deleted_at', '<', Carbon::now()->subDays(30))->limit(1000)->delete();
            sleep(1);
        } while ($deleted > 0);
    }
}
