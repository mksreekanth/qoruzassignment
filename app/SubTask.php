<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubTask extends Model
{
    const FIELD_NAME_TITLE = "title";
    const FIELD_NAME_DUE_DATE = "due_date";
    const FIELD_NAME_STATUS = "status";

    /**
     * Get the task that owns the subtask.
     */
    public function parentTask()
    {
        return $this->belongsTo('App\Task');
    }
}
